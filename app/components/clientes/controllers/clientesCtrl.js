var clientesCtrl = function () {

	console.log("run clients");
	var clientesCont = $('#clientes');
	
	
	var btnListado 	 = clientesCont.find('#btnListar');
	var btnCrear 	 = clientesCont.find('#btnCrear');

	
	var clientesArr  = [];
	var clientesMl   = $.get(API + "models/clientes.json");
	var listadoView  = $.get(VIEWS + "clientes/views/indexView.html");
	var formView 	 = $.get(VIEWS + "clientes/views/formView.html");

	listadoView.then( function (listadoVw){
		clientesCont.append(listadoVw);

		formView.then( function (formVw){
			clientesCont.append(formVw);

			var listado 	 = clientesCont.find('#listado');
			var crearEditar  = clientesCont.find('#crear--modificar');
			var tbody 	     = listado.find('tbody');
			var form    	 = crearEditar.find('form');
			var btnGuardar 	 = crearEditar.find('#guardar');

			btnListado.click( function () {
				crearEditar.hide();

				// clientesMl  = $.get(API + "models/clientes.json");
				clientesMl.then( function (res){
					if(clientesArr.length == 0){
						clientesArr = res;
					}
					

					// ELIMINAR TBODY CONTENT
					tbody.find('tr').remove();

					// APPEND
					for (var i = clientesArr.length - 1; i >= 0; i--) {
						tbody.append('<tr id="splice-'+clientesArr[i].id+'"><td>'+clientesArr[i].id+'</td><td>'+clientesArr[i].nombre+'</td><td>'+clientesArr[i].email+'</td><td>'+clientesArr[i].cedula+'</td><td><button class="btn btn-danger btn--eliminar" id="cliente-'+clientesArr[i].id+'">Eliminar</button></td></tr>')
					}
					
					var btnEliminar = tbody.find('.btn--eliminar');
					
					btnEliminar.click( function (){
						var id = $(this).attr('id').split('-')[1];
						console.log("boton Eliminar: ", id);
						// clientesArr.splice(id,1);

						clientesArr.map( function (item, position){
							if(item.id == id){
								clientesArr.splice(position, 1);
								tbody.find('#splice-'+id).remove();
								console.log("item elegido: ", item, position);
							}
						})

						// PROGRAMATIC CLICK
						// btnListado.click();
						return false;
					})

					listado.show();
					return false;
				})
			})

			btnGuardar.click( function (){
				var id 			= form.find('#id').val();
				var nombre 		= form.find('#nombre').val();
				var email 		= form.find('#email').val();
				var cedula 		= form.find('#cedula').val();

				if(!id){
					alert("Error: id vacia");
					return false;
				}

				console.log("form: ", id, nombre, email, cedula);

				var postArr 	= {
					id: id,
					nombre: nombre,
					email: email,
					cedula: cedula
				};

				// clientesArr.push(postArr);

				console.log("postArr: ", clientesArr);

				$.ajax({
				  type: "POST",
				  url: API + "models/clientes_post.json",
				  data: JSON.stringify(postArr),
				  dataType: 'json',
				  contentType: 'application/json',
				}).then( function (res) {
					if(res.success == true){
						clientesArr = res.clients;
					}
				});

				return false;
			});

			btnCrear.click( function (){
				listado.hide();
				crearEditar.show();
			});
		})
	})


	

	

	
	console.log("clientesArr: ", clientesArr);

}