var API 	= 'api/';
var VIEWS 	= 'app/components/';
var globalCont = $('#global--container');

var mainCtrl = function () {
	var menu 	= $('#menu');
	var link 	= menu.find('a');
	var abms 	= $('.abm');

	abms.hide();

	link.click( function (){
		var href = $(this).attr('href');
		abms.hide();
		$(href).show();
		console.log("ev: click", href);
	})
}

mainCtrl();

// RUN CLIENTS
clientesCtrl();
loginCtrl();